function s = L(x , i , n )

s = '(' ;

for j = 0 : n
   if(j ~= i)
       
       tempi = num2str(x(i+1));
       tempj = num2str(x(j+1));
       
       if(s == '(')
           s0 = ' ((x - ' ;
       else
           s0 = ' .* ((x - ' ;
       end
       s = [s , s0 , tempj , ') / (' , tempi , '-' , tempj , ' ))'] ;
   end
end
s = [s , ')'];
end