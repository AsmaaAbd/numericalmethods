function [validEntry,notificationString,tab,time]=bisectionMethod(func,x_l,x_u,es,i_max)
validEntry=1;
time=0;
tab =cell(i_max,8);
notificationString='no errors';
L(1) = x_l;
U(1) =x_u;
ya(1)= feval(inline(func),L(1));
yb(1) = feval(inline(func),U(1));
ea(1)=1/0;
%---------------------------------------

%---------------------------------------
tab(1,1)=num2cell(L(1));
tab(1,2)=num2cell(ya(1));
tab(1,3)=num2cell(U(1));
tab(1,4)=num2cell(yb(1));
tab(1,7)=num2cell(es);
if((ya(1)*yb(1))>0)
    validEntry=0;
    notificationString ='Function has same sign at end points';
    disp('Function has same sign at end points');
    return
end

disp('Iterations     xl        xu        xr        es        f(xr)');
tic;
for i=1:i_max
    Sol(i) = (U(i)+L(i))/2;
    y(i)=feval(inline(func),Sol(i));
    
    tab(i,1)=num2cell(L(i));
    tab(i,3)=num2cell(U(i));
    tab(i,2)=num2cell(ya(i));
    tab(i,4)=num2cell(yb(i));
    tab(i,5)=num2cell(Sol(i));
    tab(i,6)=num2cell(y(i));
    %-*------------------------------------------
    fig=figure;
    hax=axes;
    syms x
    A = sym(func);
    b = matlabFunction(A);
    x = -1:.1:1;
    plot(x,b(x))
    hold on ;    
    sp1 = L(i);
    sp2 = U(i);
    y1=get(gca,'ylim');
    line([sp1 sp1],y1, 'color',rand(1,3))
    line([sp2 sp2],y1, 'color',rand(1,3))
    pause 
    close
    %-*------------------------------------------
    
    
    if y(i) == 0.0
        notificationString='exact zero found';
        disp('exact zero found');
        break;
    elseif y(i)*yb(i) <0
        L(i+1)=Sol(i);
        ya(i+1)=y(i);
        U(i+1)=U(i);
        yb(i+1)=yb(i);
    else
        L(i+1)=L(i);
        ya(i+1)=ya(i);
        U(i+1)=Sol(i);
        yb(i+1)=y(i);
    end;
    
    if (i>1)
        ea(i)=abs(Sol(i) - Sol(i-1))/Sol(i);
        tab(i,7)=num2cell(ea(i));
        if (ea(i) <es)
            notificationString='reached desired value of tolerance , bracketing method has converged';
            disp('bracketing method has converged');break;
        end
    end
    iter=i;
end
time =toc;

if(iter >= i_max)
    notificationString='Maximum iterations reached and desired tolerance isn''t reached yet';
    disp('zero not found to desired tolerance');
end;


n=length(Sol);k=1:n;out=[k;L(1:n);U(1:n);Sol;ea(1:n);y];
disp   ('itertions      xl            xu            es            xr            f(xr)');
fprintf('%d              %f      %f     %f      %f      %E\n',out);
end