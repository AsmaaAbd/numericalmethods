function outp=bisection(func,x_l,x_u,es,i_max)



a(1) = x_l;
tab =cell(50,6);
 tab(1,1)=num2cell(a(1));
b(1) =x_u;
 tab(1,3)=num2cell(b(1));
ya(1)= feval(inline(func),a(1));
 tab(1,2)=num2cell(ya(1));
yb(1) = feval(inline(func),b(1));
 tab(1,4)=num2cell(yb(1));
ea(1)=1/0;
es=es;
 tab(1,5)=num2cell(es);
i_max=i_max;


if((ya(1)*yb(1))>0)
    disp('Function has same sign at end points');
    return
end

disp('Iterations     xl        xu        xr        es        f(xr)');
tic;
for i=1:i_max
     tab(i,1)=num2cell(a(i));
      tab(i,3)=num2cell(b(i));
       tab(i,2)=num2cell(ya(1));
        tab(i,4)=num2cell(yb(1));
    x(i) = (b(i)+a(i))/2;
    y(i)=feval(inline(func),x(i));
    % x_new=x_mid;
    % test=f(x_l)*f(x_mid);
    if y(i) == 0.0
        disp('exact zero found');break;
    elseif y(i)*yb(i) <0
        a(i+1)=x(i);
        ya(i+1)=y(i);
        b(i+1)=b(i);
        yb(i+1)=yb(i);
    else
        a(i+1)=a(i);
        ya(i+1)=ya(i);
        b(i+1)=x(i);
        yb(i+1)=y(i);
    end;
    
    if (i>1)
        ea(i)=abs(x(i) - x(i-1))/x(i);
        if (ea(i) <es)
            disp('bracketing method has converged');break;
        end
    end
    iter=i;
end
toc;

if(iter >= i_max)
    disp('zero not found to desired tolerance');
end


n=length(x);k=1:n;out=[k;a(1:n);b(1:n);x;ea(1:n);y];
disp   ('itertions      xl            xu            es            xr            f(xr)');
fprintf('%d              %f      %f     %f      %f      %E\n',out);
outp =tab;
end