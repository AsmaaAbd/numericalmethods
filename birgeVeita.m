function  [validEntry,notiString , itTable,time]=birgeVeita(func,initial,eps,maxit)  % latest version
time=0;
tic
validEntry=1;
notiString='no errors;';
itTable=cell(maxit,6);
  fig=figure;
A = sym(func);
b = matlabFunction(A);
x = -10:.1:10;
plot(x,b(x),'color','red')
co=sym2poly(sym(func))
b=0 ;
sol=0;
c=0;
power=length(co)
sol(1)= initial
b(1)= co(1);
c(1)= co(1);
for k=2 :maxit 
    itTable(k,1)=num2cell(sol(k-1));
    for i=2 : power
        b(i)=b(i-1)*sol(k-1)+ co(i)
    end
    for i=2 :power-1
        c(i)=c(i-1)*sol(k-1)+b(i)
    end
    sol(k)= sol(k-1)-b(power)/c(power-1)
    itTable(k,5)=num2cell(sol(k));
     itTable(k,6)=num2cell(abs(sol(k)-sol(k-1)));
    if abs(sol(k)-sol(k-1))<=eps
        break ;
    end
   time= toc
end
