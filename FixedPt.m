function [x,y,warning,gOfX,itTable,time]=FixedPt(func, x0, es, iter_max) 
warning='no error';
time=0;
tic
itTable =cell( iter_max,8);
x(1) = x0;
y(1) = feval(inline(func),x(1));
ea(1)=1/0;
%es=str2double(es);
 itTable(1,3)=num2cell(ea(1));
 itTable(1,1)=num2cell(x(1));
 itTable(1,2)=num2cell(y(1));
 
double xr_old;
 iter=2;
%temp = @(x) x;
display(isstr(func));
g= [func,' + x'] 
display(isstr(g));

gOfX=g;
%g=@(x)g;
while(iter < iter_max)
    xr_old = x(iter-1);
     x(iter) = feval(inline(g),xr_old); 
          itTable(iter-1,1)=num2cell(x(iter-1));
              % itTable(iter,2)=num2cell(y(iter));
 itTable(iter-1,2)=num2cell(x(iter));
% itTable(iter,3)=num2cell(y(iter+1));
 
     if (x(iter) == 0)
         warning='real root is reached';
         break; 
     end
     ea(iter) = abs((x(iter) - xr_old) / x(iter));
         itTable(iter,3)=num2cell(ea(iter));
    iter=iter+1;
    if(ea(iter-1) < es  )
        warning='desired tolerance is reached';
        break;
    end    
end 
n=length(x);k=1:n;out=[k;x(1:n);ea(1:n)];
disp('Iterations     xr        es');    
fprintf('%d              %f      %f\n',out);
    
end
