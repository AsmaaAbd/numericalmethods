function [f, coeffs, dividedDiff] = runNewtonInterEquallyspaced (x,y,p,n)
disp('in');
tic;
 [f, coeffs, dividedDiff]= newtonInterpol(x,y,p,n);
disp('in222');
toc;
hold on;
    fig2 = figure(2);
    set (fig2, 'Units', 'normalized', 'Position', [0 0 0.5 0.5]);
plot(x,y,'ob');
plot(p,f,'-r');
title('Newton Interpolation');
legend('samples', 'interpolation');
disp(f);
disp(coeffs);
disp(dividedDiff);
