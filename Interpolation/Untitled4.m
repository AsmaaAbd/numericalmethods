function [ interpolatedFunc, coeffs, dividedDiff ] = newtonInterpol( x, y, q, n )
%newtonInterpol Newton divided difference for interpolation

%  [ interpolatedFunc coeffs dividedDiff ] = newtonInterpol(x, y, p, n)
%
% Input arguments :
%  x   (vector) of size 1xN which contains the interpolation sample points.
%  y   (vector) of size 1xN which contains the corresponding values at x
%  q   (vector) of size 1xQueryPointsNumber which contains points to be interpolated. 
%  n   order of the polynomial
%
% Output arguments :
%  interpolatedFunc   (vector) of size 1xP. The result of interpolation respect to query.
%  [coeffs] (vector) of size 1xN which is leading coefficients genereated by 
%       divided difference method.
%  [dividedDiff] (matrix) of size NxN (triangular) which is the result of the 
%       divided difference method
%
% Example
% >> x=[1,2,4,7,8]
% >> y=inline('x+x^2 + x^3');
% >> [f a d]= newtoninter(x, y, 5)
tic;
dividedDiff(:,1)=y'; %building the 2D array initially with 1st column equal to the givin f(xi)
for j = 2 : n
    for i = j : n
        dividedDiff(i,j)= ( dividedDiff(i-1,j-1)-dividedDiff(i,j-1)) / (x(i-j+1)-x(i));
    end
end
coeffs = diag(dividedDiff)';
query(1,:) = repmat(1, size(q));
s(1,:) = repmat(coeffs(1), size(q));
for j = 2 : n
   query(j,:)=(q - x(j-1)) .* query(j-1,:);
   s(j,:) = coeffs(j) .* query(j,:);
end
interpolatedFunc=sum(s);
toc;

